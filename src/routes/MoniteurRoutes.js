import React from 'react';
import {Routes, Route} from 'react-router-dom'
import useConnectedUser from "../hooks/useConnectedUser";
import DashboardMoniteur from "../Components/Dashboard/DashboardMoniteur/DashboardMoniteur";



export default function AuthenticatedBaseRoutes(){

    const {type} = useConnectedUser();

    return (
        <Routes>

            <Route path="/" exact element={<DashboardMoniteur/>}/>


        </Routes>
    )
}
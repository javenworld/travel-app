import React from 'react';
import {Routes, Route} from 'react-router-dom'
import useConnectedUser from "../hooks/useConnectedUser";
import DashboardMoniteur from "../Components/Dashboard/DashboardMoniteur/DashboardMoniteur";
import DashboardAdmin from "../Components/Dashboard/DashboardAdmin/DashboardAdmin";
import SideBar from "../Components/Dashboard/SideBar/SideBar";
import MenuAdmain from "../Components/Dashboard/DashboardAdmin/MenuAdmin/MenuAdmain";
import DashboardPage from "../pages/Authenticated/Admin/DashboardPage";
import UserList from "../pages/Authenticated/Admin/UserList/UserList";
import useUserHasRoles from "../hooks/useUserHasRoles";
import CommandeListPage from "../pages/Authenticated/Admin/Commandes/CommandeListPage";
import CommandeDetailsPage from "../pages/Authenticated/Admin/Commandes/CommandeDetailsPage/CommandeDetailsPage";
import CompagniesPage from "../pages/Authenticated/Admin/Compagnies/CompagniesPage";


export default function AdminRoutes() {

    const userHasRoles = useUserHasRoles();

    return (
        <Routes>
            <Route path="/" exact element={<DashboardPage/>}/>

            {
                userHasRoles(['super-admin']) && (
                    <Route path="/users" exact element={<UserList/>}/>
                )
            }


            <Route path={'/commandes'} exact element={<CommandeListPage/>}/>
            <Route path={'/commandes/:commandeId'} exact element={<CommandeDetailsPage/>}/>

            <Route path={'/villes'} exact element={<CompagniesPage/>}/>

        </Routes>
    )
}
import React from 'react';
import {Routes, Route, Navigate} from 'react-router-dom'
import useConnectedUser from "../hooks/useConnectedUser";
import DashboardMoniteur from "../Components/Dashboard/DashboardMoniteur/DashboardMoniteur";
import AdminRoutes from "./AdminRoutes";
import ClientRoutes from "./ClientRoutes";
import {useDispatch} from "react-redux";
import {deconnectUser} from "../stores/authStore";
import AdminTemplate from "../Components/Templates/AdminTemplate";

function LogoutUser(){

    const dispatch = useDispatch();

    React.useEffect(()=>{
        dispatch(deconnectUser())
    },[])

    return null;
}

export default function AuthenticatedBaseRoutes(){

    const {type} = useConnectedUser();

    return (
        <div>
            <Routes>

                {
                    type != 'client' && (
                        <Route path={'/*'} element={<AdminTemplate />} />
                    )
                }

                {
                    type == 'client' && (
                        <Route path={'/*'} element={<ClientRoutes />} />
                    )
                }

                <Route path={'*'} element={<LogoutUser />}/>

            </Routes>
        </div>

    )
}
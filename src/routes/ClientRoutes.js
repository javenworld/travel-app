import React from 'react';
import {Routes, Route} from 'react-router-dom'
import useConnectedUser from "../hooks/useConnectedUser";
import DashboardMoniteur from "../Components/Dashboard/DashboardMoniteur/DashboardMoniteur";
import DashboardAdmin from "../Components/Dashboard/DashboardAdmin/DashboardAdmin";
import UserDashboard from "../Components/UserDashboard/UserDashboard";



export default function ClientRoutes(){

    return (
        <Routes>
            <Route path="/*" exact element={<UserDashboard/>}/>
        </Routes>
    )
}
const UPDATE_USER_DATABASE = "fakeDatabaseStore/UPDATE_USER_DATABASE";
const UPDATE_COMMANDE_DATABASE = "fakeDatabaseStore/UPDATE_COMMANDE_DATABASE";
const UPDATE_VILLES_DATABASE = "fakeDatabaseStore/UPDATE_VILLES_DATABASE";
const UPDATE_TRAJETS_DATABASE = "fakeDatabaseStore/UPDATE_TRAJETS_DATABASE";
const UPDATE_COMPAGNIES_DATABASE = "fakeDatabaseStore/UPDATE_COMPAGNIES_DATABASE";

const initialState = {
    users : [
        {
            id: 1,
            name: 'Zebato',
            surname: 'Nadia',
            email: 'admin@gmail.com',
            phone: '+22512345678',
            password: 'azerty',
            confirmPassword: 'azerty',
            type: 'administrateur',
            roles: ['super-admin', 'admin', 'moniteur']
        }
    ],
    commandes: [],
    villes: [],
    trajets: [],
    compagnies: [],
};

export function updateUsersDatabaseAction(users){
    return function(dispatch,state){
        dispatch({
            type: UPDATE_USER_DATABASE,
            users
        });
    }
}


export function updateCompagniesDatabaseACtion(compagnies){
    return function(dispatch,state){
        dispatch({
            type: UPDATE_COMPAGNIES_DATABASE,
            compagnies
        });
    }
}

export function updateCommandesDatabaseAction(commandes){
    return function(dispatch,state){
        dispatch({
            type: UPDATE_COMMANDE_DATABASE,
            commandes
        });
    }
}


export function updateVillesDatabaseAction(villes){
    return function(dispatch,state){
        dispatch({
            type: UPDATE_VILLES_DATABASE,
            villes
        });
    }
}


export function updateTrajetsDatabaseAction(trajets){
    return function(dispatch,state){
        dispatch({
            type: UPDATE_TRAJETS_DATABASE,
            trajets
        });
    }
}


export default function fakeDatabaseStore(state = initialState , action){

    switch (action.type) {

        case UPDATE_USER_DATABASE:
            return {
                ...state,
                users : action.users
            };

        case UPDATE_COMMANDE_DATABASE:
            return {
                ...state,
                commandes : action.commandes
            };

        case UPDATE_VILLES_DATABASE:
            return {
                ...state,
                villes : action.villes
            };

        case UPDATE_TRAJETS_DATABASE:
            return {
                ...state,
                trajets : action.trajets
            };

        case UPDATE_COMPAGNIES_DATABASE:
            return {
                ...state,
                compagnies : action.compagnies
            };

        default: return state;

    }

};

import {createStore, combineReducers, applyMiddleware, compose} from "redux"
import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';
import {persistStore, persistReducer} from 'redux-persist';
import authStore from "./authStore";
import storage from 'redux-persist/lib/storage'
import fakeDatabaseStore from "./fakeDatabaseStore";

let combinedStores = combineReducers(
    {
        authStore,
        fakeDatabaseStore,
    }
);

// @ts-ignore
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['authStore', 'fakeDatabaseStore'],
    timeout: null
};

const rootPersistConfig = {
    key: 'root',
    storage,
    timeout: null
};

const persistedStore = persistReducer(persistConfig, combinedStores);

const store = createStore(persistedStore,
    composeWithDevTools(applyMiddleware(thunk)));

const persistor = persistStore(store);

// persistor.purge();

export {persistor};
export default store;





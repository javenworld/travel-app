const ACTION_UPDATE_USER = "appStateStore/ACTION_UPDATE_USER";
const ACTION_USER_CONNECTED = "appStateStore/ACTION_USER_CONNECTED";
const ACTION_USER_DECONNECTED = "appStateStore/ACTION_USER_DECONNECTED";


const initialState = {
    user : undefined,
};

export function connectUser(userData){
    return function(dispatch,state){
        dispatch({
            type: ACTION_USER_CONNECTED,
            user : userData
        });
    }
}

export function updateUser(userData){
    return function(dispatch,state){
        dispatch({
            type: ACTION_UPDATE_USER,
            user : userData
        });
    }
}

export function deconnectUser(){
    return function(dispatch,state){
        dispatch({
            type: ACTION_USER_DECONNECTED,
        });
    }
}

export default function authStore(state = initialState , action){

    switch (action.type) {

        case ACTION_USER_CONNECTED:
            return {
                ...state,
                user : action.user
            };

        case ACTION_UPDATE_USER:
            return {
                ...state,
                user : action.user
            };

        case ACTION_USER_DECONNECTED:
            return {
                ...state,
                user : undefined
            };

        default: return state;

    }

};

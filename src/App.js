import './App.css';
import Nav from './Components/Nav/Nav';
import Home from './Components/Home/Home'
import About from './Components/About/About'
import Login from './Components/Login/Login'
import SignUp from './Components/SignUp/SignUp'
import UserSession from './Components/UserSession/UserSession'
import UserDashboard from './Components/UserDashboard/UserDashboard'

import SideBar from '../src/Components/Dashboard/SideBar/SideBar'
import DashboardAdmin from '../src/Components/Dashboard/DashboardAdmin/DashboardAdmin'
import DashboardMoniteur from '../src/Components/Dashboard/DashboardMoniteur/DashboardMoniteur'
import OrdersItems from '../src/Components/Dashboard/OrdersItems/OrdersItems'
import MenuMoniteur from '../src/Components/Dashboard/DashboardMoniteur/MenuMoniteur/MenuMoniteur'

import {Routes, Route, Navigate} from 'react-router-dom'
import Footer from './Components/Footer/Footer';
import MenuAdmain from './Components/Dashboard/DashboardAdmin/MenuAdmin/MenuAdmain';
import UserRequestPending from '../src/Components/UserRequestPending/UserRequestPending'
import useConnectedUser from "./hooks/useConnectedUser";
import AuthenticatedBaseRoutes from "./routes/AuthenticatedBaseRoutes";
import SignUpAdmin from "./Components/SignUpAdmin/SignUpAdmin";



function App() {

  const isConnected = !!useConnectedUser();


  return (
    <div className="App">

        {/* <Nav/> */}
        <Routes basename={process.env.PUBLIC_URL}>
          <Route path="/Nav" exact element={<Nav/>}/>
          <Route path="/" exact element={<Home/>}/>
          <Route path="/About" exact element={<About/>}/>
          <Route path="/UserSession" exact element={<UserSession/>}/>
          <Route path="/OrdersItems" exact element={<OrdersItems/>}/>
          <Route path="/MenuMoniteur" exact element={<MenuMoniteur/>}/>
          <Route path="/MenuAdmin" exact element={<MenuAdmain/>}/>
          <Route path="/UserRequestPending" exact element={<UserRequestPending/>}/>
          <Route path="/Footer" exact element={<Footer/>}/>

          {
            !isConnected && (
                <>
                  <Route path="/Login" exact element={<Login/>}/>
                  <Route path="/SignUp" exact element={<SignUp/>}/>
                  <Route path="/signup-admin" exact element={<SignUpAdmin />}/>
                  <Route
                      path="*"
                      element={<Navigate to="/" />}
                  />
                </>
            )
          }

          {
            !!isConnected && (
                <Route path={'/dashboard/*'} element={<AuthenticatedBaseRoutes />} />
            )
          }



          <Route
              path="*"
              element={<Navigate to="/" />}
          />

        </Routes>
        {/* <Footer/> */}

    </div>
  );
}

export default App;

import store from "../stores";
import {
    updateCommandesDatabaseAction, updateCompagniesDatabaseACtion, updateTrajetsDatabaseAction,
    updateUsersDatabaseAction,
    updateVillesDatabaseAction
} from "../stores/fakeDatabaseStore";
import moment from "moment";

class FakeDatabase{

    getUsersDatabase(){
        return store.getState().fakeDatabaseStore.users;
    }

    getCommandeDatabase(){
        return store.getState().fakeDatabaseStore.commandes;
    }

    getVillesDatabase(){
        return store.getState().fakeDatabaseStore.villes;
    }

    getTrajetDatabase(){
        return store.getState().fakeDatabaseStore.trajets;
    }

    getCompagnieDatabase(){
        return store.getState().fakeDatabaseStore.compagnies
    }

    updateUserDatabase(users){
        store.dispatch(updateUsersDatabaseAction(users))
    }

    updateCommandeDatabase(commandes){
        store.dispatch(updateCommandesDatabaseAction(commandes))
    }
    updateCompagniesDatabase(compagnies){
        store.dispatch(updateCompagniesDatabaseACtion(compagnies))
    }

    updateVilleDatabase(villes){
        store.dispatch(updateVillesDatabaseAction(villes))
    }

    updateTrajetDatabases(trajets){
        store.dispatch(updateTrajetsDatabaseAction(trajets))
    }

    addUser(user){

        const users = this.getUsersDatabase();

        const existingUser = users.find(
            (userItem) => {
                if(userItem.email == user.email)
                    return userItem;
            }
        )

        if(existingUser)
            throw new Error('deja-inscrit');

        const newUserData = {
                id: users.length + 1,
                ...user
            };

        this.updateUserDatabase([
            ...users,
            newUserData
        ])

        return newUserData;
    }

    authUser(email, password){

        const users = this.getUsersDatabase();

        const user = users.find(
            (userItem) => {
                if(userItem.email == email && userItem.password == password)
                    return userItem;
            }
        )

        if(!user)
            throw new Error('auth-error')

        return user;
    }

    getUsers(){
        const users = this.getUsersDatabase();

        return users;
    }

    getUser(userId){

        const users = this.getUsersDatabase();

        const user = users.find(
            (userItem) => {
                if(userItem.id == userId)
                    return userItem;
            }
        )

        return user;
    }

    removeUser(){

    }

    addCommande(commande, userId){

        const commandes = this.getCommandeDatabase();

        const commandeData = {
            id: commandes.length,
            ...commande,
            userId,

        }

        this.updateCommandeDatabase([
            ...commandes,
            commandeData
        ])

        return commandeData;
    }

    getUserCommande(userId){

        const commandes = this.getCommandeDatabase().filter(
            commande => {
                return commande.userId == userId
            }
        )

        return commandes;
    }

    getCommandes({states, userId} = {}){
        const commandes = this.getCommandeDatabase().filter(
            commande => {

                let bool = true;

                if(states){
                    bool = bool && states.includes(bool)
                }

                if(userId)
                    bool = bool && commande.userId == userId

                return bool;
            }
        ).map(
            commande => {
                const client = this.getUser(commande.userId)

                return {
                    ...commande,
                    client
                }

            }
        )

        return commandes;
    }

    getCommande(commandeId){

        const commande = this.getCommandeDatabase().find(
            commande => {
                return commande.id == commandeId;
            }
        );

        const client = this.getUser(commande.userId)

        return {
            ...commande,
            client
        };
    }

    updateCommandeState(commandeId, userId, newCommandeState){

        const commandes = this.getCommandeDatabase();
        const validator = this.getUser(userId);

        this.updateCommandeDatabase([
            ...commandes.map(
                item => {
                    if(item.id == commandeId){
                        return {
                            ...item,
                            state: newCommandeState,
                            validedDate: moment().format('YYYY-MM-DD HH:mm'),
                            monitorValidator: userId,
                            validator
                        }
                    }

                    return item;
                }
            ),
        ])

    }

    getVilles(){
        return this.getVillesDatabase();
    }


    getCompagnies(){
        return this.getCompagnieDatabase();
    }

    addCompagnie(ville){

        const compagnies = this.getCompagnieDatabase();

        const comagnieData = {
            id: compagnies.length + 1,
            ...ville
        }

        this.updateCompagniesDatabase([
            ...compagnies,
            comagnieData
        ])

        return comagnieData;
    }

    deleteCompagny(compagnyId){

        const compagnies = this.getCompagnieDatabase();

        this.updateCompagniesDatabase([
            ...compagnies.filter(
                item => {
                    return item.id != compagnyId
                }
            ),
        ])

    }

    updateCompagny(compagny){

        const compagnies = this.getCompagnieDatabase();

        this.updateCompagniesDatabase([
            ...compagnies.map(
                item => {
                    if(compagny.id == item.id)
                        return compagny
                    return item;
                }
            ),
        ])

    }

    addTrajet(depart, arrive){

        const trajets = this.getTrajetDatabase();

        const trajetData = {
            id: trajets.length,
            departId: depart.id,
            arriveId: arrive.id
        }

        this.updateVilleDatabase([
            ...trajets,
            trajetData
        ])

        return trajetData;

    }

    updateMultipleTrajets(departId, arrives){

        const trajets = this.getTrajetDatabase();

        const newTrajets = arrives.map(
            (arriveId, index) => ({
                id: trajets.length + index + 1,
                departId: departId,
                arriveId: arriveId
            })
        )

        console.log('new trajets', newTrajets)

        this.updateTrajetDatabases([
            ...trajets.filter(
                item => item.departId != departId
            ),
            ...newTrajets
        ])

    }

    deleteTrajet(trajetId){

    }

    getTrajets(departId){

        const trajets = this.getTrajetDatabase();

        return trajets.filter(
            item => item.departId == departId
        )
    }



}


const fakeDatabase = new FakeDatabase();

export default fakeDatabase;
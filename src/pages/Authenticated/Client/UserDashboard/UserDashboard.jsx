import React, {useState} from 'react'
import {Link} from 'react-router-dom'
import './UserDashboard.css'
import { Route, Routes } from 'react-router'
import ProfilPage from './Profil/ProfilPage'
import HistoriquePage from './Historique/HistoriquePage'

function UserDashboard() {

    return (
        <>
            <div className="user-dashboard">
                <nav className="user-dashboard-nav">
                    <div className="nav-left">
                        <span>Entreprise</span>
                        <div className="user-img"></div>
                        <span>Username</span>
                        <span>✔</span>
                    </div>
                    <div className="logout">
                        <span>Logout</span>
                    </div>
                </nav>

                <div className="user-dashboard-body" style={{display: 'flex'}}>
                    <div className="user-dashboard-sidebar">
                        <ul>
                            <div className="li-top">
                                <div className="user-dash">
                                    <li>Tableau de bord</li>
                                </div>
                                <div className="user-profil">
                                    <li><a href="/UserDashboard/profil">Mon profil</a></li> 
                                </div>
                                <div className="user-history">
                                    <li>
                                    <a href="/UserDashboard/historique">Mon historique</a></li>
                                </div>
                            </div>
                            <div className="buy"><li><Link to="/">Acheter un ticket</Link></li></div>
                            <div className="li-bot"><li>Deconnexion</li></div>
                        </ul>
                    </div>

                    <div style={{flex: 1}}>
                        
                        <Routes>

                            <Route path="profil" element={<ProfilPage />} />
                            <Route path="historique" element={<HistoriquePage />} />

                        </Routes>

                    </div>

                </div>
            </div>
        </>
    )
}

export default UserDashboard

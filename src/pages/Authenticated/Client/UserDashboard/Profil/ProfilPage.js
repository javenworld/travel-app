import React from 'react';


export default function ProfilPage(){


    return (
       
        <div className={`profil-items addUserProfil`}>
            <div className="profil-top">
                <div className="user-infos">INFORMATIONS PERSONNNELLES</div>
                <div className="user-edit">MODIFIER LES INFOS</div>
            </div>
            <div className="profil-bot">
                <form>
                    <div className="name-surname">
                        <label>Nom & Prénom(s) : </label>
                        <input type="text" />
                    </div>
                    <div className="email">
                        <label>Email : </label>
                        <input type="text" />
                    </div>
                    <div className="telephone">
                        <label>Numéro de téléphone : </label>
                        <input type="text" />
                    </div>
                    <button>Modifier</button>
                </form>
            </div>
        </div>
    )
}
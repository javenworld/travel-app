import React from 'react';
import DashboardResumeComponent from "../../../Components/Dashboard/DashboardResumeComponent";
import fakeDatabase from "../../../FakeDatabase/FakeDatabase";


export default function DashboardPage(props){


    return (
        <div>
            <div className="cards">

                <DashboardResumeComponent icon={'👥'} asyncGetNumber={async ()=>{
                    return fakeDatabase.getUsers().length
                }} title={'Clients'}   />

                <DashboardResumeComponent icon={'📦'} asyncGetNumber={async ()=>{
                    return 999
                }} title={'Commandes'}   />

                <DashboardResumeComponent icon={'💷'} asyncGetNumber={async ()=>{
                    return 999
                }} title={'Tickets'}   />

                <DashboardResumeComponent icon={'⌚'} asyncGetNumber={async ()=>{
                    return 999
                }} title={'Heure & Date'}   />

            </div>

            <div className="recent-flex">
                <div className="projets">
                    <div className="card">
                        <div className="card-header">
                            <h2>Toutes les commandes</h2>
                            <span>📦</span>
                        </div>
                        <div className="card-body">
                            <table style={{width: "100%"}}>
                                <thead>
                                <tr>
                                    <td>Client</td>
                                    <td>Téléphone</td>
                                    <td>Nombre de tickets</td>
                                    <td>Localisation</td>
                                    <td>Destination</td>
                                    <td>Date de voyage</td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                </tr>
                                <tr>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                </tr>
                                <tr>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                </tr>
                                <tr>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                </tr>
                                <tr>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                </tr>
                                <tr>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                </tr>
                                <tr>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                </tr>
                                <tr>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                    <td>Lorem ipsum dolor sit amet.</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
import React from 'react';
import fakeDatabase from "../../../../../FakeDatabase/FakeDatabase";
import {useParams} from "react-router";
import CommandeStateUpdater from "../../../../../Components/Commandes/CommandeStateUpdater/CommandeStateUpdater";
import CommandeBadge from "../../../../../Components/Commandes/CommandeBadge";
import ValidationInfo from "../../../../../Components/Commandes/ValidationInfo";
import useVilleData from "../../../../../hooks/datas/useVilleData";
import AuthGuard from "../../../../../Components/Auth/AuthGuard";


export default function CommandeDetailsPage(){

    const {commandeId} = useParams();
    const [commande, setCommande] = React.useState();

    const {villes} = useVilleData();
    function findVille(villeId){
        return villes.find(item => item.id == villeId)
    }

    function loadCommande(){

        const commande = fakeDatabase.getCommande(commandeId)
        setCommande(commande);
    }

    React.useState(()=>{
        loadCommande()
    }, [])

    return (
        <div>

            {
                commande && (
                    <div className={'row'}>

                        <div className={'col-4'}>

                            <div className="card">
                                <div className="card-body">

                                    <div className="text-center">
                                        <CommandeBadge state={commande.state} />
                                    </div>


                                    <div className={'mt-3'}>Commande Ref</div>
                                    <h3 className={'font-weight-bold'}>xxx-xxx-xxx</h3>


                                    <div className="mt-6">
                                        <CommandeStateUpdater shouldReload={()=>{loadCommande()}} commande={commande} />
                                    </div>

                                </div>
                            </div>


                            <div className="card card-custom border-1 border-black mt-2">
                                <div className="card-body bg-white">

                                    <div>Client</div>

                                    <h5 className={'font-weight-bold'}>{commande.client.name} {commande.client.surname}</h5>

                                    <div></div>

                                </div>
                            </div>

                            <AuthGuard allowedRoles={['super-admin', 'administrateur']}>

                                {
                                    commande.validedDate && (
                                        <ValidationInfo commande={commande} />
                                    )
                                }
                                
                            </AuthGuard>


                        </div>

                        <div className="col-8">

                            {
                                commande.tickets.map(
                                    (ticket, index) => {
                                        return (
                                            <div className={'card bg-white'}>

                                                <div className={'card-body'}>

                                                    <div className="row">

                                                        <div className="col-12 h3 text-left">
                                                            Tickets ({ticket.ticket} exemplaires) #{index + 1}
                                                        </div>
                                                        <div className="col-3">
                                                            <div className={'small'}>Depart</div>
                                                            <div className={'h5 font-weight-boldest'}>{ticket.city}</div>
                                                        </div>

                                                        <div className="col-3">
                                                            <div>Arrivée</div>
                                                            <div className={'h5 font-weight-boldest'}>{ticket.destination}</div>
                                                        </div>

                                                        <div className="col-3">
                                                            <div>Compagnie</div>
                                                            <div className={'h5 font-weight-boldest'}>{ticket.company}</div>
                                                        </div>

                                                        <div className="col-3">
                                                            <div>Date</div>
                                                            <div className={'h5 font-weight-boldest'}>{ticket.date}</div>
                                                        </div>

                                                        <div className="col-12">
                                                            <hr/>
                                                        </div>

                                                        <div className="col-3">
                                                            <div className={'small'}>Nom</div>
                                                            <div className={'h5 font-weight-boldest'}>{ticket.name}</div>
                                                        </div>

                                                        <div className="col-3">
                                                            <div>Prenoms</div>
                                                            <div className={'h5 font-weight-boldest'}>{ticket.surname}</div>
                                                        </div>

                                                        <div className="col-3">
                                                            <div>Téléphone</div>
                                                            <div className={'h5 font-weight-boldest'}>{ticket.phone}</div>
                                                        </div>

                                                        <div className="col-3">
                                                            <div>Localisation</div>
                                                            <div className={'h5 font-weight-boldest'}>{ticket.location}</div>
                                                        </div>

                                                        <div className="divider"></div>

                                                    </div>

                                                </div>

                                            </div>
                                        )
                                    }
                                )
                            }

                        </div>

                    </div>
                )
            }

        </div>
    )
}
import React from 'react';
import fakeDatabase from "../../../../FakeDatabase/FakeDatabase";
import ReactPaginate from "react-paginate";
import Pagination from "react-js-pagination";


export default function UserList() {

    const [isLoading, setIsLoading] = React.useState(false);

    const [usersPage, setUsersPage] = React.useState();

    function loadUsers(page) {

        try {

            const users = fakeDatabase.getUsers();

            setUsersPage({
                items: users,
                currentPage: page,
                totalPage: 1
            })

        }catch (e) {
            console.log(e)
        }

    }

    React.useEffect(()=>{
        loadUsers()
    }, [])

return (
    <div>

        <table className="table table-custom">

            <thead>
                <tr>
                    <th>Nom & Prenoms</th>
                    <th>Email</th>
                    <th>Téléphone</th>
                    <th>Type</th>
                    <th>Roles</th>
                    <th>Action</th>
                </tr>
            </thead>

            <tbody>

            {usersPage && usersPage.items && usersPage.items.length > 0 && usersPage.items.map(
                user => (
                    <tr>
                        <th>{user.name} {user.surname}</th>
                        <th>{user.email}</th>
                        <th>{user.phone}</th>
                        <th>{user.type}</th>
                        <th>{user.roles.map(
                            role => (
                                <span className="badge bg-secondary d-inline-block me-1">{role}</span>
                            )
                        )}</th>
                        <th><div>
                            Actions sur l'utilisateur
                        </div></th>
                    </tr>
                )
            )}

            {usersPage && usersPage.items && usersPage.items.length == 0 && (
                    <tr>
                        <td colSpan={6}>
                            <div>Aucun utilisateur enregistré</div>
                        </td>
                    </tr>

            )}

            </tbody>

        </table>


        {usersPage && (
            <Pagination
                itemClass="page-item"
                linkClass="page-link"
                activePage={usersPage.currentPage}
                itemsCountPerPage={25}
                totalItemsCount={1}
                pageRangeDisplayed={5}
                onChange={() => {}}
            />
        )}

    </div>
);
}
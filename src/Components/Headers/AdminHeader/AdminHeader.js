import React from 'react';
import './admin-header.css'
import useConnectedUser from "../../../hooks/useConnectedUser";


export default function AdminHeader(){

    const user = useConnectedUser();

    return (
        <header>

            <h2>
                <label htmlFor="">
                    <span>📰</span>
                </label>
                Dashboard
            </h2>

            <div className="search-wrapper">
                <span>🏸</span>
                <input type="search" placeholder='search..'/>
            </div>
            <div className="notification-wrapper">
                <a href="">
                    <span>🔔</span>
                </a>
                <ul className={`notif-items addNotif-items-todelete`} style={{position : 'absolute'}}>
                    <h3>Notifications</h3>
                    <li>Lorem ipsum dolor sit amet.</li>
                    <li>Lorem ipsum dolor sit amet.</li>
                    <li>Lorem ipsum dolor sit amet.</li>
                    <li>Lorem ipsum dolor sit amet.</li>
                </ul>
            </div>

            <div className="user-wrapper">
                <img src="" alt="" />
                <div>
                    <h4>{user.name} {user.surname}</h4>
                    <small>Admin</small>
                </div>
                <div>
                    <span>✔</span>
                    <div className={`user-minisession add-user-minisession`}>
                        <p>Profil😊</p>
                        <p>Setting⚙</p>
                        <p>Logout🩸</p>
                    </div>
                </div>
            </div>
        </header>
    )
}
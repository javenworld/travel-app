import React from 'react'
import {Link} from 'react-router-dom'
import {useState} from 'react'
import clsx from 'clsx';

import './SideBar.css'
import AuthGuard from "../../Auth/AuthGuard";
import useRouteMatcher from "../../../hooks/useRouteMatcher";

function SideBar() {

    const [drowpdownItem, setDrowpdownItem] = useState(false)

    const routeMatcher = useRouteMatcher();

    function handleClick(e) {
        e.preventDefault()
        setDrowpdownItem(!drowpdownItem);
    };

    return (
        <>
            <div className="sidebar">
                <div className="sidebar-brand">
                    <h2><span>🏡</span>Entreprise</h2>
                </div>

                <div className="sidebar-menu">
                    <ul>
                        <li>
                            <Link to="/dashboard" className='active'>
                                <span>📰</span>
                                <span>Tableau de bord</span>
                            </Link>
                        </li>
                        <AuthGuard allowedRoles={['super-admin']}>

                            <li tabIndex={0} className={clsx(['profil position-relative'])}>
                                <Link to="/dashboard/users" className={clsx([{'active': routeMatcher("/dashboard/users")}])}>
                                    <span>👩‍👩‍👦‍👦</span>
                                    <span>Profils et Utilisateurs</span>
                                </Link>
                                <div className={clsx(['sub-menu d-none', {'d-block': drowpdownItem}])}>
                                    <ul>
                                        <Link to="/DashboardAdmin">
                                            <li>Administrateur</li>
                                        </Link>
                                        <Link to="/DashboardMoniteur">
                                            <li>Moniteur</li>
                                        </Link>
                                    </ul>
                                </div>
                            </li>
                        </AuthGuard>
                        <li tabIndex={0} className={clsx(['profil position-relative'])}>
                            <Link to="/dashboard/commandes">
                                <span>👩‍👩‍👦‍👦</span>
                                <span>Commandes</span>
                            </Link>
                        </li>
                        <li tabIndex={0} className={clsx(['profil position-relative'])}>
                            <Link to="/dashboard/villes">
                                <span>👩‍👩‍👦‍👦</span>
                                <span>Compagnies</span>
                            </Link>
                        </li>
                        <AuthGuard allowedRoles={['super-admin']}>
                            <>
                                <li>
                                    <a href="">
                                        <span>⚙</span>
                                        <span>Parametres</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <span>🔑</span>
                                        <span>Configurations</span>
                                    </a>
                                </li>
                            </>
                        </AuthGuard>

                        <li>
                            <a href="">
                                <span>❔</span>
                                <span>Aide</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>


        </>
    )
}

export default SideBar
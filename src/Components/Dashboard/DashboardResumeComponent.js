import React from 'react';


export default function DashboardResumeComponent(props){

    const {number, asyncGetNumber, title, icon, requiredRoles} = props
    const [computedNumber, setComputedNumber] = React.useState();
    const [computeNumberError, setComputeNumberError] = React.useState();

    React.useEffect(()=>{

        if(asyncGetNumber){
            asyncGetNumber().then(
                result => setComputedNumber(result)
            ).catch(e => setComputeNumberError(e))
        }

    }, [asyncGetNumber])

    return (
        <div className="card-single">
            <div>
                <h1>
                    {
                        !asyncGetNumber && (
                            number
                        )
                    }
                    {
                        !!asyncGetNumber && computedNumber != undefined && (
                            computedNumber
                        )
                    }
                </h1>
                <span>{title}</span>
            </div>
            <div>
                <span>{icon}</span>
            </div>
        </div>
    );
}
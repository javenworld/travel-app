import React , {useState} from 'react'
import { Link } from 'react-router-dom'
import OrdersItems from '../../OrdersItems/OrdersItems'
import './MenuMoniteur.css'

import Login from '../../../Login/Login'

function MenuMoniteur() {

    const [notif, setNotif] = useState(false)

    const classToggle = (e) => {
        e.preventDefault()
        setNotif(!notif)
    }

    const [userMinisession, setUserMinisession] = useState(false)

    const classToggleUserMinisession = (e) => {
        e.preventDefault()
        setUserMinisession(!userMinisession)
    }

    return (
        <div>

<div className="main-content">
                <header>

                    <h2>
                        <label htmlFor="">
                            <span>📰</span>
                        </label>
                        Dashboard
                    </h2>

                    <div className="search-wrapper">
                        <span>🏸</span>
                        <input type="search" placeholder='search..'/>
                    </div>
                    <div className="notification-wrapper" onClick={classToggle}>
                        <a href="">
                            <span>🔔</span>
                        </a>
                        <ul className={`notif-items ${notif ? 'addNotif-items' : ''}`} style={{position : 'absolute'}}>
                            <h3>Notifications</h3>
                            <li>Lorem ipsum dolor sit amet.</li>
                            <li>Lorem ipsum dolor sit amet.</li>
                            <li>Lorem ipsum dolor sit amet.</li>
                            <li>Lorem ipsum dolor sit amet.</li>
                        </ul>
                    </div>

                    <div className="user-wrapper">
                        <img src="" alt="" />
                        <div>
                            <h4>Nadi</h4>
                            <small>Admin</small>
                        </div>
                        <div onClick={classToggleUserMinisession}>
                            <span>✔</span>
                            <div className={`user-minisession ${userMinisession ? 'add-user-minisession' : ''}`}>
                                <p>Profil😊</p>
                                <p>Setting⚙</p>
                                <p>Logout🩸</p>
                            </div>
                        </div>
                    </div>
                </header>

                
                <main>
                    <div className="cards">
                        <div className="card-single">
                            <div>
                                <h1>546</h1>
                                <span>Commandes</span>
                            </div>
                            <div>
                                <span>📦</span>
                            </div>
                        </div>
                        <div className="card-single">
                            <div>
                                <h1>578</h1>
                                <span>Clients</span>
                            </div>
                            <div>
                                <span>👥</span>
                            </div>
                        </div>
                        <div className="card-single">
                            <div>
                                <h1>854</h1>
                                <span>Tickets</span>
                            </div>
                            <div>
                                <span>💷</span>
                            </div>
                        </div>
                    </div>

                    <div className="recent-flex">
                        <div className="projets">
                            <div className="card">
                                <div className="card-header">
                                    <h2>Toutes les commandes</h2>
                                    <span>📦</span>
                                </div>
                                <div className="card-body">
                                    <table style={{width: "100%"}}>
                                        <thead>
                                            <tr>
                                                <td>Client</td>
                                                <td>Téléphone</td>
                                                <td>Nombre de tickets</td>
                                                <td>Localisation</td>
                                                <td>Destination</td>
                                                <td>Date de voyage</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                            </tr>
                                            <tr>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                            </tr>
                                            <tr>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                            </tr>
                                            <tr>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                            </tr>
                                            <tr>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                            </tr>
                                            <tr>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                            </tr>
                                            <tr>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                            </tr>
                                            <tr>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                                <td>Lorem ipsum dolor sit amet.</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            
        </div>
    )
}

export default MenuMoniteur

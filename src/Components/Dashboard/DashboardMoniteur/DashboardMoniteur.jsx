import React from 'react'
import {Link} from 'react-router-dom'
import MenuMoniteur from '../DashboardMoniteur/MenuMoniteur/MenuMoniteur'
import SideBar from '../SideBar/SideBar'

function DashboardMoniteur() {
    return (
        <div>
            <SideBar />
            <MenuMoniteur/>
        </div>
    )
}

export default DashboardMoniteur

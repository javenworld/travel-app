import React , {useState} from 'react'
import './MenuAdmin.css'
import AdminHeader from "../../../Headers/AdminHeader/AdminHeader";
import AdminRoutes from "../../../../routes/AdminRoutes";



function MenuAdmain() {

    const [notif, setNotif] = useState(false)
    
    const classToggle = (e) => {
        e.preventDefault()
        setNotif(!notif)
    }

    const [userMinisession, setUserMinisession] = useState(false)

    const classToggleUserMinisession = (e) => {
        e.preventDefault()
        setUserMinisession(!userMinisession)
    }


    return (
        <div>
            
            <div className="main-content">

                <AdminHeader />

                <main>

                    <AdminRoutes />

                </main>

            </div>
        </div>
    )
}

export default MenuAdmain

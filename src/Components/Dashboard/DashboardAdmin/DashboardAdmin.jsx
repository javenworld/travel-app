import React from 'react'
import {Link} from 'react-router-dom'
import SideBar from '../SideBar/SideBar'
import MenuAdmain from '../DashboardAdmin/MenuAdmin/MenuAdmain'

function DashboardAdmin() {
    return (
        <div>
            <SideBar/>
            <MenuAdmain/>
        </div>
    )
}

export default DashboardAdmin

import React from 'react'
import './OrdersItems.css'

function OrdersItems() {
    return (
        <div className='container-ordersItems'>
            <div className="orders">
                <div className="order">
                    <span>Nom du client : Lorem ipsum dolor sit amet.</span>
                    <span>Numero de télephone : </span>
                    <span>Nombre de tichet : </span>
                    <span>Localisation : </span>
                    <span>Destination :</span>
                    <span>Date de voyage : </span>
                </div>

                <div className="order">
                    <span>Nom du client : </span>
                    <span>Numero de télephone : </span>
                    <span>Nombre de tichet : </span>
                    <span>Localisation : </span>
                    <span>Destination :</span>
                    <span>Date de voyage : </span>
                </div>

                <div className="order">
                    <span>Nom du client : </span>
                    <span>Numero de télephone : </span>
                    <span>Nombre de tichet : </span>
                    <span>Localisation : </span>
                    <span>Destination :</span>
                    <span>Date de voyage : </span>
                </div>

                <div className="order">
                    <span>Nom du client : </span>
                    <span>Numero de télephone : </span>
                    <span>Nombre de tichet : </span>
                    <span>Localisation : </span>
                    <span>Destination :</span>
                    <span>Date de voyage : </span>
                </div>

                <div className="order">
                    <span>Nom du client : </span>
                    <span>Numero de télephone : </span>
                    <span>Nombre de tichet : </span>
                    <span>Localisation : </span>
                    <span>Destination :</span>
                    <span>Date de voyage : </span>
                </div>
            </div>
        </div>
    )
}

export default OrdersItems

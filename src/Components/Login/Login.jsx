import React, {useState} from 'react'
import { Link } from 'react-router-dom'
import { v4 as uuidv4 } from 'uuid';

import './Login.css'
import Nav from '../Nav/Nav'
import Footer from '../Footer/Footer'
import fakeDatabase from "../../FakeDatabase/FakeDatabase";
import {useDispatch} from "react-redux";
import {connectUser} from "../../stores/authStore";

function Login() {

    const dispatch = useDispatch();

    const [login, setLogin] = useState([]);
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();

    const [loginError, setLoginError] = useState();

    function handleChangeEmailLogin(e) {
        e.preventDefault()
        setEmail(e.target.value)
    }

    function handleChangePasswordLogin(e) {
        e.preventDefault()
        setPassword(e.target.value)
    }

    function doLogin(email, password){

        try {

            const user = fakeDatabase.authUser(email, password);

            console.log('user data', user)

            dispatch(connectUser(user));

            setLoginError()

        } catch (e) {
            console.log(e)
            setLoginError(e)
        }

    }

    function handleSubmit(e) {
        e.preventDefault()
        if(email && password) {
            // setLogin([...login, {
            //     id : uuidv4(),
            //     email : email,
            //     password : password
            // }])

            doLogin(email, password)

        }
        else {
            alert("No no")
        }
        // setLogin("")
        // setEmail("")
        // setPassword("")
    }



    return (
        <>
            <Nav/>
            <div className="container-connexion">

                <div className="container-form">
                    <h1>Renseignez vos identifiants !</h1>
                    <p>Vous êtes nouveau ? 
                        <Link to="/SignUp">Creez un compte</Link>
                    </p>
                    <div className="container-avatar">
                        <div className="avatar"></div>
                    </div>

                    <form onSubmit={handleSubmit}>

                        {
                            loginError && loginError.message == 'auth-error' && (
                                <div className={'text-center'}>
                                    <span className="text-danger h5 font-weight-bold">Informations incorrectes !</span>
                                </div>
                            )
                        }


                        <input type="email" placeholder="Email" onChange={handleChangeEmailLogin}/>
                        <input type="password" placeholder="Mot de passe" onChange={handleChangePasswordLogin}/>
                        <p>Se souvenir de moi <input type="checkbox" name="" id="" /></p>
                        <a href="#">Mot de passe oublié ?</a>
                        {/* <Link to="/UserDashboard"> */}
                            <button className='btn-connexion'>Connexion</button>
                        {/* </Link> */}
                    </form>
                </div>
        
            </div>

            <Footer/>
        </>
    )
}

export default Login

import React from 'react';
import useConnectedUser from "../../hooks/useConnectedUser";


export default function AuthGuard(props){

    const {allowedRoles, children} = props;
    const user = useConnectedUser();

    const allow = React.useMemo(()=>{

        for(let role of user.roles){

            if(allowedRoles.includes(role))
                return true;

        }

        return false
    }, [user, allowedRoles])

    if(allow)
        return children;

    return null;
}
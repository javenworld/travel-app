import React from 'react'
import {Link} from 'react-router-dom'
import { useState } from 'react'
import { v4 as uuidv4 } from 'uuid';
import './Home.css'
import Nav from '../Nav/Nav';
import Footer from '../Footer/Footer';
import {useNavigate} from 'react-router-dom';

import busVille from '../assets/bus-ville.webp'
import iphoneFrance from '../assets/iphone-france.png'

import {useSelector} from 'react-redux'
import fakeDatabase from "../../FakeDatabase/FakeDatabase";
import useConnectedUser from "../../hooks/useConnectedUser";
import useVilleData from "../../hooks/datas/useVilleData";
import useTrajetsData from "../../hooks/datas/useTrajetsData";
import useCompagniesData from "../../hooks/datas/useCompagniesData";


function Home() {

    const navigate = useNavigate();

    const {villes} = useVilleData();
    function findVille(villeId){
        return villes.find(item => item.id == villeId)
    }

    const [userInputs, setUserInputs] = useState([]);
    const [clientName, setClientName] = useState();
    const [clientSurname, setClientSurname] = useState();
    const [clientPhone, setClientPhone] = useState();
    const [clientLocalisation, setClientLocalisation] = useState();
    const [clientDepartureCity, setClientDepartureCity] = useState();
    const [clientDestination, setClientDestination] = useState();
    const [clientCompanyChoice, setCompanyChoice] = useState();
    const [ticketNumber, setTicketNumber] = useState();
    const [clientDate, setClientDate] = useState();

    const user = useConnectedUser();

    const {compagnies} = useCompagniesData();

    function handleChangeClientName(e) {
        setClientName(e.target.value);
        console.log(e.target.value);
    }

    function handleChangeClientSurname(e) {
        setClientSurname(e.target.value)
    }
    function handleChangeClientPhone(e) {
        setClientPhone(e.target.value)
    }
    function handleChangeClientLocalisation(e) {
        setClientLocalisation(e.target.value)
    }
    function handleChangeClientDepartureCity(e) {
        setClientDepartureCity(e.target.value)
    }
    function handleChangeClientDestination(e) {
        setClientDestination(e.target.value)
    }
    function handleChangeCompanyChoice(e) {
        setCompanyChoice(e.target.value)
    }

    function handleChangeTicketNumber(e) {
        setTicketNumber(e.target.value)
    }

    function handleChangeClientDate(e) {
        setClientDate(e.target.value)
    }

    function handleSubmit(e) {
        e.preventDefault()
        if(clientName && clientSurname && clientPhone && clientLocalisation && clientDepartureCity 
            && clientDestination && clientCompanyChoice && ticketNumber && clientDate){

            const selectedCompagny = compagnies.find(compagny => compagny.id == clientCompanyChoice);
            const prix = selectedCompagny.data[clientDepartureCity][clientDestination]

            setUserInputs([...userInputs, {
                id: uuidv4(),
                name: clientName,
                surname : clientSurname,
                phone : clientPhone,
                location : clientLocalisation,
                city : clientDepartureCity,
                destination : clientDestination,
                company : selectedCompagny.label,
                ticket : ticketNumber,
                date : clientDate,
                total: prix * parseInt(ticketNumber)
            }])
            console.log(clientDepartureCity)
            
            
        }else{
            alert("Veuillez renseignez tous les champs !")
            console.log(userInputs)
        }

        // setClientName("");
        // setClientSurname("");
        // setClientLocalisation("");
        // setClientPhone("");
        // setClientDepartureCity("");
        // setClientDestination("");
        // setCompanyChoice("");
        // setTicketNumber("");
        // setClientDate("");
    }

    async function makeReservation(){

        try {

            const commande = fakeDatabase.addCommande({
                state: 'pending',
                tickets: userInputs,
                date: (new Date()).getTime()
            }, user.id)

            navigate('/UserRequestPending', {
                commande
            })

        } catch (e) {
            console.log(e)
        }

    }

    const phrase = useSelector(state => state.phrase)

    const departs = React.useMemo(
        ()=>{
            return Object.keys(compagnies.find(
                compagnie => {
                    return compagnie.id == clientCompanyChoice;
                }
            )?.data || {});
        }, [clientCompanyChoice]
    );

    const arrives = React.useMemo(
        ()=>{

            console.log('Home arrives', Object.fromEntries(
                Object.entries(compagnies.find(
                    compagnie => {
                        return compagnie.id == clientCompanyChoice;
                    }
                )?.data[clientDepartureCity] || {})
            ));

            return Object.fromEntries(
                Object.entries(compagnies.find(
                    compagnie => {
                        return compagnie.id == clientCompanyChoice;
                    }
                )?.data[clientDepartureCity] || {})
            );
            }, [clientDepartureCity, clientCompanyChoice]
    );

    const selectedDestinationPrix = React.useMemo(
        ()=>{
            return arrives[clientDestination]
            }, [clientDestination, arrives]
    );

    console.log('departs', departs);

    return (
        <>
            <Nav/>
            <div className="container-home-page">
                {/*<strong> <h2>{phrase}</h2></strong> */}

                <div className="home-page-title">
                    
                    <h1>Entreprise vous facilite votre voyage</h1>
                </div>

                <div className="home-page-container-input px-3 py-2">
                    <div className="btn-go-return">
                        <button className="btn-go">Allez simple</button>
                        <button className="btn-return">Allez retour</button>
                    </div>

                    <form onSubmit={handleSubmit}>

                        <div className="row user-informations travel-informations">

                            <div className="col-lg-6 col-12">
                                <input type="text" placeholder="Votre nom" className="user-name input-home-page" onChange={handleChangeClientName}/>
                            </div>

                            <div className="col-lg-6 col-12">
                                <input type="text" placeholder="Votre Prénom(s)" className="user-name input-home-page" onChange={handleChangeClientSurname}/>
                            </div>

                            <div className="col-lg-6 col-12">
                                <input type="text" placeholder="Votre numéro de téléphone" className="user-contact input-home-page" onChange={handleChangeClientPhone}/>
                            </div>

                            <div className="col-lg-6 col-12">
                                <input type="text" placeholder="Votre localisation ?" className="user-location input-home-page" onChange={handleChangeClientLocalisation}/>
                            </div>

                            <div className="col-lg">
                                <select className="user-compagnie select-home-page" onChange={handleChangeCompanyChoice}>
                                    <option >Compagnie ?</option>
                                    {
                                        compagnies && compagnies.map(
                                            compagnie => (
                                                <option value={compagnie.id}>{compagnie.label}</option>
                                            )
                                        )
                                    }
                                </select>
                            </div>
                            <div className="col-lg">
                                <select className="user-departure-city select-home-page" onChange={handleChangeClientDepartureCity}>
                                    <option >Ville de départ ?</option>
                                    {
                                        departs && departs.map(
                                            ville => (
                                                (
                                                    <option value={ville}>{ville}</option>
                                                )
                                            )
                                        )
                                    }
                                </select>
                            </div>
                            <div className="col-lg">
                                <select className="user-destination select-home-page" onChange={handleChangeClientDestination}>
                                    <option >Destination ?</option>
                                    {
                                        arrives && Object.entries(arrives).map(
                                            ([label, prix]) => (
                                                (
                                                    <option value={label}>{label}</option>
                                                )
                                            )
                                        )
                                    }
                                </select>
                            </div>
                            <div className="col-lg">
                                <input type="number" placeholder="Nombre de passager(s) ?" className="user-ticket input-home-page" onChange={handleChangeTicketNumber}/>
                            </div>
                            <div className="col-lg">
                                <input type="date" placeholder="Date depart ?" className="user-date input-home-page" onChange={handleChangeClientDate}/>
                            </div>

                        </div>

                        {/* <p>Afficher les montants des trajets ici</p> */}
                        <button className="btn btn-success btn-block w-100" id="btn">Ajouter</button>

                        {
                            selectedDestinationPrix && (
                                <div className={'text-center'}>

                                    <div>Coût du trajet</div>
                                    <div className={'h3 font-weight-bolder'}>{selectedDestinationPrix} F CFA</div>

                                </div>
                            )
                        }

                        <p id="message"></p>
                    </form>
                </div>

                {
                    userInputs && userInputs.length > 0 && (
                        <div className={'container mt-5 pt-5'}>

                            <div className="row bg-secondary px-5 py-2 align-items-center">
                                <div className="col">
                                    <div className="border-radius text-white h4">
                                        Panier
                                    </div>
                                </div>
                                <div className="col-auto">
                                    <button onClick={()=>{
                                        makeReservation()
                                    }} className="btn btn-success">Reservez</button>
                                </div>
                            </div>
                        </div>
                    )
                }

                {userInputs.map((userInput, index) => {
                    return (
                        <>
                        <div className="user-add" key={userInput.id}>
                            <ul className='user-travel-add'>
                                <li><strong>Nom :</strong> {userInput.name}</li>
                                <li><strong>Prénom :</strong>  {userInput.surname}</li>
                                <li><strong>Téléphone :</strong> {userInput.phone}</li>
                                <li><strong>Location actuelle :</strong> {userInput.location}</li>
                                <li><strong>Ville de départ :</strong> {userInput.city}</li>
                                <li><strong>Destination :</strong>{userInput.destination}</li>
                                <li><strong>Compagnie de préférence :</strong> {userInput.company}</li>
                                <li><strong>Nombre de passager(s) :</strong> {userInput.ticket}</li>
                                <li><strong>Date de voyage :</strong> {userInput.date}</li>
                                <li><strong>Total :</strong> {userInput.total} F CFA</li>
                            </ul>
                            <button onClick={()=>{
                                setUserInputs(
                                    userInputs.filter(
                                        (item, itemIndex) => itemIndex !== index
                                    )
                                )
                            }} className={'btn btn-danger'}>
                                Rétirer
                            </button>
                            {/*<Link to="/UserRequestPending">*/}
                            {/*    <button className="btn" id="btn">Réserver</button>*/}
                            {/*</Link>*/}
                        </div>
                        </>
                    )
                })}

                <div className="business-brief-description">
                    <div className="descriptions">
                        <img src={busVille} alt="" />

                        <div className="description-text">
                            <h2>Entreprise</h2>
                            <p>Avec nous pas besoin de vous deplacer pour vos achats de ticket de voyage👌.
                                Nous nous chargeons de vous acheter le ticket et vous le livrer😊 !
                            </p>
                        </div>
                    </div>
                </div>
                
                <div className="mobile-app">
                    <div className="elements-app">
                        <div className="app-image"><img src={iphoneFrance} alt="" /></div>
                        <div className="app-text">
                            <h2>Lorem ipsum dolor sit amet.</h2>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium, ullam corporis, ut doloribus quibusdam repellendus dolorum ipsum labore iste, soluta vitae. Quasi officia quaerat voluptates, ipsum consequuntur blanditiis deleniti. Magni earum nisi iusto excepturi aut sed hic, ex quia pariatur tempora obcaecati quisquam expedita, laboriosam commodi incidunt dicta officiis repudiandae quos similique. Impedit est minima aut porro qui nulla libero, obcaecati hic enim quam nihil tenetur debitis nemo labore, molestias reiciendis a, atque exercitationem? Nobis at dicta odio, dolores possimus est, optio voluptatum temporibus dignissimos ratione laborum nam delectus illum suscipit? Consequatur ut nostrum velit officiis veniam distinctio quas accusamus!</p>
                        </div>
                    </div>
                </div>
                
            </div>

            <Footer/>
        </>
    )
}

export default Home

import React from 'react'
import "./UserSession.css"

function UserSession() {
    return (
        <>
            <div className="container-user-session">

                <div className="recapitulatif">
                    <div className="content-left">
                        <div className="container-recap">
                            <h2>Recapitulatif</h2>
                            <div className="user-recapitulatif">
                                <span>Nom :</span>
                                <span>Prénom(s) :</span>
                                <span>Numero de telephone :</span>
                                <span>Localisaion :</span>
                                <span>Type de voyage :</span>
                                <span>Ville de départ : </span>
                                <span>Ville d'arriver : </span>
                                <span>Compagnie : </span>
                                <span>Nombre de ticket : </span>
                                <span>Date départ :</span>
                            </div>
                        </div>

                    </div>

                    <div className="content-right">
                        <div className="container-content-right">
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur impedit quas alias quaerat fuga odit, dolores officia. Praesentium, at in. Voluptates, culpa odio minus fugit earum adipisci deserunt rem suscipit saepe dolorem, impedit error nobis quasi. Culpa exercitationem officiis facilis corporis porro aspernatur expedita tempora praesentium? Quod veniam asperiores magni totam necessitatibus ex! Beatae laudantium aut, laborum nobis minima reprehenderit. Nisi, nobis assumenda, quaerat alias blanditiis itaque molestiae praesentium repellendus aperiam non autem vero. Sapiente at tenetur vero iusto deserunt quia aperiam nobis assumenda odio fugiat enim asperiores qui error minima quidem, a, quae animi? Dolorum quo minus cum odit?
                        </div>
                        <div className="container-content-right">
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur impedit quas alias quaerat fuga odit, dolores officia. Praesentium, at in. Voluptates, culpa odio minus fugit earum adipisci deserunt rem suscipit saepe dolorem, impedit error nobis quasi. Culpa exercitationem officiis facilis corporis porro aspernatur expedita tempora praesentium? Quod veniam asperiores magni totam necessitatibus ex! Beatae laudantium aut, laborum nobis minima reprehenderit. Nisi, nobis assumenda, quaerat alias blanditiis itaque molestiae praesentium repellendus aperiam non autem vero. Sapiente at tenetur vero iusto deserunt quia aperiam nobis assumenda odio fugiat enim asperiores qui error minima quidem, a, quae animi? Dolorum quo minus cum odit?
                        </div>
                        <div className="container-content-right">
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur impedit quas alias quaerat fuga odit, dolores officia. Praesentium, at in. Voluptates, culpa odio minus fugit earum adipisci deserunt rem suscipit saepe dolorem, impedit error nobis quasi. Culpa exercitationem officiis facilis corporis porro aspernatur expedita tempora praesentium? Quod veniam asperiores magni totam necessitatibus ex! Beatae laudantium aut, laborum nobis minima reprehenderit. Nisi, nobis assumenda, quaerat alias blanditiis itaque molestiae praesentium repellendus aperiam non autem vero. Sapiente at tenetur vero iusto deserunt quia aperiam nobis assumenda odio fugiat enim asperiores qui error minima quidem, a, quae animi? Dolorum quo minus cum odit?
                        </div>
                        <div className="container-content-right">
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur impedit quas alias quaerat fuga odit, dolores officia. Praesentium, at in. Voluptates, culpa odio minus fugit earum adipisci deserunt rem suscipit saepe dolorem, impedit error nobis quasi. Culpa exercitationem officiis facilis corporis porro aspernatur expedita tempora praesentium? Quod veniam asperiores magni totam necessitatibus ex! Beatae laudantium aut, laborum nobis minima reprehenderit. Nisi, nobis assumenda, quaerat alias blanditiis itaque molestiae praesentium repellendus aperiam non autem vero. Sapiente at tenetur vero iusto deserunt quia aperiam nobis assumenda odio fugiat enim asperiores qui error minima quidem, a, quae animi? Dolorum quo minus cum odit?
                        </div>
                        <div className="container-content-right">
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur impedit quas alias quaerat fuga odit, dolores officia. Praesentium, at in. Voluptates, culpa odio minus fugit earum adipisci deserunt rem suscipit saepe dolorem, impedit error nobis quasi. Culpa exercitationem officiis facilis corporis porro aspernatur expedita tempora praesentium? Quod veniam asperiores magni totam necessitatibus ex! Beatae laudantium aut, laborum nobis minima reprehenderit. Nisi, nobis assumenda, quaerat alias blanditiis itaque molestiae praesentium repellendus aperiam non autem vero. Sapiente at tenetur vero iusto deserunt quia aperiam nobis assumenda odio fugiat enim asperiores qui error minima quidem, a, quae animi? Dolorum quo minus cum odit?
                        </div>
                        <div className="container-content-right">
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur impedit quas alias quaerat fuga odit, dolores officia. Praesentium, at in. Voluptates, culpa odio minus fugit earum adipisci deserunt rem suscipit saepe dolorem, impedit error nobis quasi. Culpa exercitationem officiis facilis corporis porro aspernatur expedita tempora praesentium? Quod veniam asperiores magni totam necessitatibus ex! Beatae laudantium aut, laborum nobis minima reprehenderit. Nisi, nobis assumenda, quaerat alias blanditiis itaque molestiae praesentium repellendus aperiam non autem vero. Sapiente at tenetur vero iusto deserunt quia aperiam nobis assumenda odio fugiat enim asperiores qui error minima quidem, a, quae animi? Dolorum quo minus cum odit?
                        </div>
                        <div className="container-content-right">
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur impedit quas alias quaerat fuga odit, dolores officia. Praesentium, at in. Voluptates, culpa odio minus fugit earum adipisci deserunt rem suscipit saepe dolorem, impedit error nobis quasi. Culpa exercitationem officiis facilis corporis porro aspernatur expedita tempora praesentium? Quod veniam asperiores magni totam necessitatibus ex! Beatae laudantium aut, laborum nobis minima reprehenderit. Nisi, nobis assumenda, quaerat alias blanditiis itaque molestiae praesentium repellendus aperiam non autem vero. Sapiente at tenetur vero iusto deserunt quia aperiam nobis assumenda odio fugiat enim asperiores qui error minima quidem, a, quae animi? Dolorum quo minus cum odit?
                        </div>
                        <div className="container-content-right">
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur impedit quas alias quaerat fuga odit, dolores officia. Praesentium, at in. Voluptates, culpa odio minus fugit earum adipisci deserunt rem suscipit saepe dolorem, impedit error nobis quasi. Culpa exercitationem officiis facilis corporis porro aspernatur expedita tempora praesentium? Quod veniam asperiores magni totam necessitatibus ex! Beatae laudantium aut, laborum nobis minima reprehenderit. Nisi, nobis assumenda, quaerat alias blanditiis itaque molestiae praesentium repellendus aperiam non autem vero. Sapiente at tenetur vero iusto deserunt quia aperiam nobis assumenda odio fugiat enim asperiores qui error minima quidem, a, quae animi? Dolorum quo minus cum odit?
                        </div>
                        <div className="container-content-right">
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur impedit quas alias quaerat fuga odit, dolores officia. Praesentium, at in. Voluptates, culpa odio minus fugit earum adipisci deserunt rem suscipit saepe dolorem, impedit error nobis quasi. Culpa exercitationem officiis facilis corporis porro aspernatur expedita tempora praesentium? Quod veniam asperiores magni totam necessitatibus ex! Beatae laudantium aut, laborum nobis minima reprehenderit. Nisi, nobis assumenda, quaerat alias blanditiis itaque molestiae praesentium repellendus aperiam non autem vero. Sapiente at tenetur vero iusto deserunt quia aperiam nobis assumenda odio fugiat enim asperiores qui error minima quidem, a, quae animi? Dolorum quo minus cum odit?
                        </div>
                        <div className="container-content-right">
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur impedit quas alias quaerat fuga odit, dolores officia. Praesentium, at in. Voluptates, culpa odio minus fugit earum adipisci deserunt rem suscipit saepe dolorem, impedit error nobis quasi. Culpa exercitationem officiis facilis corporis porro aspernatur expedita tempora praesentium? Quod veniam asperiores magni totam necessitatibus ex! Beatae laudantium aut, laborum nobis minima reprehenderit. Nisi, nobis assumenda, quaerat alias blanditiis itaque molestiae praesentium repellendus aperiam non autem vero. Sapiente at tenetur vero iusto deserunt quia aperiam nobis assumenda odio fugiat enim asperiores qui error minima quidem, a, quae animi? Dolorum quo minus cum odit?
                        </div>
                    </div>

                </div>

            </div>

        </>
    )
}

export default UserSession

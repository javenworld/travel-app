import React from 'react'
import './Footer.css'

function Footer() {
    return (
        <>

            <div className="container-footer">

                <div className="container-footer-item">
                    <div className="footer-top">
                        <div className="footer-items footer-item1">
                            <h3>Adresse</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque cupiditate ipsam nemo exercitationem eius, earum vel animi. Earum, veniam repellendus.</p>
                        </div>
                        <div className="footer-items footer-item2">
                            <h3>Service client</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque cupiditate ipsam nemo exercitationem eius, earum vel animi. Earum, veniam repellendus.</p>
                        </div>
                        <div className="footer-items footer-item2">
                            <h3>Nous Suivre</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque cupiditate ipsam nemo exercitationem eius, earum vel animi. Earum, veniam repellendus.</p>
                        </div>
                    </div>
                </div>

                <div className="footer-bot">
                    <h2>[Entreprise logo]</h2>
                    <p>Confidentialité, tous droits reservé, Mentions légales © 2022</p>
                </div>

            </div>
            
        </>
    )
}

export default Footer

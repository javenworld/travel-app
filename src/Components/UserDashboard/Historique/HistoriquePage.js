import React from 'react';
import fakeDatabase from "../../../FakeDatabase/FakeDatabase";
import useConnectedUser from "../../../hooks/useConnectedUser";
import moment from "moment";
import CommandeBadge from "../../Commandes/CommandeBadge";


export default function HistoriquePage(){

    const user = useConnectedUser();
    const [histories, setHistory] = React.useState([]);

    function loadHistory(){

        const items = fakeDatabase.getUserCommande(user.id);

        setHistory(items)

    }

    React.useEffect(()=>{
        loadHistory();
    }, [])

    return (
    
        <div>
       
            <div className={`user-history-items add-user-history-items`}>

                {
                    histories && histories.map(
                        history => (
                            <div className="purchase purchase1">
                                <CommandeBadge state={history.state} />

                                <p className="date">
                                    {moment(history.date).format('DD/MM/YYYY HH:mm')}
                                </p>
                                <p className='action'><strong>Action : Achat de tickets</strong></p>
                                <p className='nbre-de-tickets'>Nombre de tickets : {history.tickets.length}.</p>
                            </div>
                        )
                    )
                }


            </div>
        </div>
    )
}
import React from 'react';
import useConnectedUser from "../../../hooks/useConnectedUser";
import {useDispatch} from "react-redux";
import {connectUser, updateUser} from "../../../stores/authStore";


export default function ProfilPage(){

    const user = useConnectedUser();
    const dispatch = useDispatch();

    const [formValues, setFormValues] = React.useState({
        name: user.name,
        surname: user.surname,
        phone: user.phone,
        email: user.email
    });

    function setValue(input, value){
        setFormValues({
            ...formValues,
            [input]: value
        })
    }

    function updateUserInfos(){

        if(formValues.name && formValues.surname && formValues.phone && formValues.email){

            dispatch(updateUser({
                ...user,
                ...formValues
            }))

        } else {

        }

    }

    return (
       
        <div className={`profil-items addUserProfil`}>
            <div className="profil-top">
                <div className="user-infos">INFORMATIONS PERSONNNELLES</div>
                <div className="user-edit">MODIFIER LES INFOS</div>
            </div>
            <div className="profil-bot">
                <form onSubmit={(e) => {

                    e.preventDefault();
                    updateUserInfos()
                }}>
                    <div className="name-surname">
                        <label>Nom: </label>
                        <input onChange={({target: {value}}) => {setValue('name', value)}} type="text" value={formValues.name} />
                    </div>
                    <div className="name-surname">
                        <label>Prenoms: </label>
                        <input onChange={({target: {value}}) => {setValue('surname', value)}} type="text" value={formValues.surname} />
                    </div>
                    <div className="email">
                        <label>Email : </label>
                        <input onChange={({target: {value}}) => {setValue('email', value)}} type="text" value={formValues.email} />
                    </div>
                    <div className="telephone">
                        <label>Numéro de téléphone : </label>
                        <input onChange={({target: {value}}) => {setValue('phone', value)}} type="text" value={formValues.phone} />
                    </div>
                    <button>Modifier</button>
                </form>
            </div>
        </div>
    )
}
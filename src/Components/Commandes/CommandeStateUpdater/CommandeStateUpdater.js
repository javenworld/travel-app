import React from 'react'
import ValidateCommande from "./ValidateCommande";
import useUserHasRoles from "../../../hooks/useUserHasRoles";


export default function CommandeStateUpdater({commande, shouldReload}){

    let Component = null;
    const userHasRoles = useUserHasRoles();

    if(commande.state == 'pending' && !userHasRoles(['moniteur']))
        return null;

    switch (commande.state) {

        case 'pending':
            Component = ValidateCommande;
            break;


    }

    return (
        <div>
            { !!Component && (
                <Component commande={commande} onDone={()=>shouldReload()} />
            )}
        </div>
    )
}
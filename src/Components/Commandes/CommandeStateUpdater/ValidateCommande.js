import React from 'react';
import fakeDatabase from "../../../FakeDatabase/FakeDatabase";
import useConnectedUser from "../../../hooks/useConnectedUser";


export default function ValidateCommande({commande, onDone}){

    const user = useConnectedUser();

    function commandeToValiderState(){
        fakeDatabase.updateCommandeState(commande.id, user.id, 'valided')
        onDone();
    }

    return (
        <button onClick={()=>{commandeToValiderState()}} className={'btn btn-danger btn-sm'}>
            VALIDER LA COMMANDE
        </button>
    )
}
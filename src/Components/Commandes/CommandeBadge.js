import React from 'react';

export default function CommandeBadge({state}){

    const texte = {
        'pending': 'En attente de traitement',
        'valided': 'Validé',
        'ticket-bought': 'Ticket achetés',
        'delivery': 'Livraison en cours',
        'delivered': 'Livré',
        'canceled': 'Commande annulé'
    }

    return (
        <button className={'badge'}>
            {texte[state]}
        </button>
    );
}
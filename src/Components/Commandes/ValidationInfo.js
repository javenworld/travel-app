import React from 'react';
import fakeDatabase from "../../FakeDatabase/FakeDatabase";


export default function ValidationInfo({commande}){

    const [monitor, setMonitor] = React.useState();

    function loadMonitor(){

        const monitor = fakeDatabase.getUser(commande.monitorValidator);
        setMonitor(monitor)

    }

    React.useEffect(()=>{
        loadMonitor();
    }, []);

    if(!monitor)
        return null;

    return (
        <div className="card card-custom border-1 border-black mt-2">
            <div className="card-body bg-white">

                <div>Validation</div>

                <h5 className={'font-weight-bold'}>{monitor.name} {monitor.surname}</h5>

                <div>Date de validation</div>

                <div className={'h5'}>
                    {commande.validedDate}
                </div>

            </div>
        </div>
    );
}
import React from 'react';
import fakeDatabase from "../../FakeDatabase/FakeDatabase";
import useUserHasRoles from "../../hooks/useUserHasRoles";
import useConnectedUser from "../../hooks/useConnectedUser";
import {Link} from "react-router-dom";
import CommandeBadge from "./CommandeBadge";

//Colonnes qui seront affiché dans le tableau si l'utilisateur a tous les droits
const columns = [
    {
        name: 'Client',
        render(commande) {
            return `${commande?.client?.name} ${commande?.client?.surname}`
        }
    },
    {
        name: 'Tickets',
        render(commande) {
            return commande.tickets.length
        }
    },
    {
        name: 'Total',
        render(commande) {
            return `XXX.XXX F CFA`
        }
    },
    {
        name: 'Etat',
        render(commande) {
            return <CommandeBadge state={commande.state} />
        }
    },
    {
        name: 'Moniteur',
        render(commande) {
            return commande.validator ? `${ commande.validator.name} ${ commande.validator.surname}` : '-'
        },
        allowedRoles: ['super-admin', 'moniteur', 'administrateur', 'admistrateur']
    },
    {
        name: 'Agent',
        render(commande){
            return commande?.agent?.name || '-'
        }
    }
];

//Actions possible sur les différentes commandes
//Action correspondant à la validation de la commande par le moniteur
//'id' n'est qu'un identifiant unique pour l'action, tandis que whereState indique que cette action est possible
//seulement si la commande est dans cet état ou ces état

const actions = [
    // {
    //     id: 'delete-commande',
    // },
    // {
    //     id: 'validate-commande',
    //     whereState: ['pending'],
    //     allowedRoles: ['moniteur']
    // },
    // {
    //     id: 'ticket-bought',
    //     whereState: ['executing'],
    //     allowedRoles: ['moniteur']
    // },
    // {
    //     id: 'delivery',
    //     whereState: ['ticket-bought'],
    //     allowedRoles: ['moniteur']
    // },
    // {
    //     id: 'delivered',
    //     whereState: ['delivery'],
    //     allowedRoles: ['moniteur']
    // },
    {
        id: 'details',
        render(commande){
            return (
                <Link to={`${commande.id}`}>
                    <button className={'btn btn-sm btn-primary'}>details</button>
                </Link>
            )
        }
    }
]

export default function AdminCommandeListTable(props) {

    const [commandes, setCommandes] = React.useState([]);

    const userHasRoles = useUserHasRoles();
    const user = useConnectedUser();


    //On filtre les colonnes qui vont s'afficher chez l'administrateur selon
    //son roles
    const allowedColumns = React.useMemo(
        () => {
            return columns.filter(
                column => {
                    //Si la colonne n'a pas de role requis, on l'affiche
                    if (!column.allowedRoles)
                        return true;

                    //si la colonne a des roles requis et que l'utilisateur a au moins un de ces roles
                    else if (column.allowedRoles && userHasRoles(column.allowedRoles))
                        return true;

                    return false;
                }
            )
        }, []
    );


    function loadCommandes() {

        const commandes = fakeDatabase.getCommandes();
        setCommandes(commandes)

        console.log('loaded commandes', commandes)

    }

    React.useEffect(() => {
        loadCommandes();
    }, [])

    return (
        <div>

            <div className="projets">
                <div className="card">
                    <div className="card-header"><h2>Toutes les commandes</h2><span>📦</span></div>
                    <div className="card-body">
                        <table className={'table'}>
                            <thead>
                            <tr>

                                {
                                    allowedColumns.map(
                                        column => (
                                            <th>{column.name}</th>
                                        )
                                    )
                                }

                                <th>
                                    Actions
                                </th>

                            </tr>
                            </thead>
                            <tbody>

                            {
                                commandes.map(
                                    commande => (
                                        <tr>

                                            {
                                                allowedColumns.map(
                                                    column => (
                                                        <td>{column.render(commande)}</td>
                                                    )
                                                )
                                            }

                                            <td>
                                                {
                                                    actions.map(
                                                        action => action.render(commande)
                                                    )
                                                }
                                            </td>

                                        </tr>
                                    )
                                )
                            }


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    )
}
import React, {useState} from 'react'
import { Link } from 'react-router-dom'
import { v4 as uuidv4 } from 'uuid';

import './SignUp.css'
import Nav from '../Nav/Nav'
import Footer from '../Footer/Footer'
import fakeDatabase from "../../FakeDatabase/FakeDatabase";
import {useDispatch} from "react-redux";
import {connectUser} from "../../stores/authStore";

function SignUp() {

    const [signUp, setSignUp] = useState([]);
    const [signUpName, setSignUpName] = useState('Kouadio');
    const [signUpSurname, setSignUpSurname] = useState('Jacob');
    const [signUpEmail, setSignUpEmail] = useState('jacoumbo@gmail.com');
    const [signUpPhone, setSignUpPhone] = useState('+22512345678');
    const [signUpPassword, setSignUpPassword] = useState('azerty');
    const [signUpPasswordConfirm, setSignUpPasswordConfirm] = useState('azerty');

    //Dispatcher utilisé mettre a jour le store redux lorsque après avoir inscrit l'utilisateur
    const dispatch = useDispatch();

    function handleChangeSignUpName(e) {
        e.preventDefault()
        setSignUpName(e.target.value)
    }
    function handleChangeSignUpSurname(e) {
        e.preventDefault()
        setSignUpSurname(e.target.value)
    }
    function handleChangeSignUpEmail(e) {
        e.preventDefault()
        setSignUpEmail(e.target.value)
    }
    function handleChangeSignUpPhone(e) {
        e.preventDefault()
        setSignUpPhone(e.target.value)
    }
    function handleChangeSignUpPassword(e) {
        e.preventDefault()
        setSignUpPassword(e.target.value)
    }
    function handleChangeSignUpPasswordConfirm(e) {
        e.preventDefault()
        setSignUpPasswordConfirm(e.target.value)
    }

    function doSignUp(userData){

        try {

            //La méthode add user nous retourne l'utilisateur nouvellement inscrit, donc on prend ses données pour le connecter immédiatement
            // en l'ajouter au store
            const user = fakeDatabase.addUser(userData)

            //La méthode connectUser est un actionCreator du store "authStore" il ajoute l'utilisateur au store
            console.log('New user', user)
            dispatch(connectUser(user))

        } catch (e) {

            console.log(e)

            if(e.message == 'deja-inscrit'){
                alert("L'utilisateur est déja inscrit")
            }

        } finally {

        }

    }

    function handleSubmit(e) {
        e.preventDefault()
        if(signUpName && signUpSurname && signUpEmail && signUpPhone && signUpPassword && signUpPasswordConfirm) {
            // setSignUp([...signUp, {
            //     name : signUpName,
            //     surname : signUpSurname,
            //     email : signUpEmail,
            //     phone : signUpPhone,
            //     password : signUpPassword,
            //     confirmPassword : signUpPasswordConfirm
            // }])


            doSignUp({
                name : signUpName,
                surname : signUpSurname,
                email : signUpEmail,
                phone : signUpPhone,
                password : signUpPassword,
                confirmPassword : signUpPasswordConfirm,
                type: 'client',
                roles: []
            })

        }
        else {
            alert("Nonoooo")
        }
    }

    return (
        <>  
            <Nav/>
            <div className="container-signUp-img">
                <div className="container-signUp">
                    <div className="formulaire">
                        <div className="form-content">
                            <h1>Rejoignez ces utilisateurs qui nous ont déjà choisi 😊!</h1>

                            <p>Vous avez déja un compte ? 
                                <Link to="/Login">Connectez-vous</Link>
                            </p>
                            <form onSubmit={handleSubmit}>
                                <input type="text" name="nom" id="" placeholder='Nom' onChange={handleChangeSignUpName}/>
                                <input type="text" name="prenom" id="" placeholder='Prénom(s)' onChange={handleChangeSignUpSurname}/>
                                <input type="text" name="email" id="" placeholder='Email' onChange={handleChangeSignUpEmail}/>
                                <input type="text" name="telephone" id="" placeholder='Téléphone' onChange={handleChangeSignUpPhone}/>
                                <input type="password" name="mdp" id="" placeholder='Mot de passe' onChange={handleChangeSignUpPassword}/>
                                <input type="password" name="mdpconfirm" id="" placeholder='Confirmation de mot de passe'
                                onChange={handleChangeSignUpPasswordConfirm}/>
                                {/* <Link to="/UserDashboard"> */}
                                    <button className='btn-inscription'>S'inscrire</button>
                                {/* </Link> */}
                            </form>
                        </div>
                    </div>
                </div>      
            </div>
            
            <Footer/>
        </>
    )
}

export default SignUp

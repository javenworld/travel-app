import React from 'react'


import './UserRequestPending.css'
import Nav from '../Nav/Nav'
import Footer from '../Footer/Footer'


function UserRequestPending() {
    return (
        <>
            <Nav/>

            <div className="container-user-request">
                <h3>🔗</h3>
                <p>Votre demande est en cours de traitements...</p>
                <p>Vous receverez un message de notification !</p>
            </div>

            <Footer/>
        </>
    )
}

export default UserRequestPending

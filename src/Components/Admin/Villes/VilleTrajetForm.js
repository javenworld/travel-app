import React from 'react';
import useVilleData from "../../../hooks/datas/useVilleData";
import fakeDatabase from "../../../FakeDatabase/FakeDatabase";


export default function VilleTrajetForm(props){

    const {compagnies: villes, selectedCompagny: compagnyProps} = props;

    const [selectedCompagny, setSelectedCompagny] = React.useState(compagnyProps);
    const [selectedDestinations, setSelectedDestinations] = React.useState({});
    const [selectedDepart, setSelectedDepart] = React.useState();

    const [departInput, setDepartInput] = React.useState();
    const [arriveInput, setArriveInput] = React.useState();
    const [trajetCoutInput, setTrajetCoutInput] = React.useState();

    console.log('selected comagny', selectedCompagny)

    const arrives = React.useMemo(
        ()=>{
            if(selectedDepart)
                return selectedCompagny.data[selectedDepart]
        }, [selectedDepart, selectedCompagny]
    )


    function addDepart(){

        setSelectedCompagny({
            ...selectedCompagny,
            data: {
                ...selectedCompagny.data,
                [departInput]: {

                }
            }
        })
    }

    function addArrive(departId, label, prix){

        setSelectedCompagny({
            ...selectedCompagny,
            data: {
                ...selectedCompagny.data,
                [departId]: {
                    ...selectedCompagny?.data[departId],
                    [label]: prix
                }
            }
        })

    }

    function deleteArrive(arriveId){

        setSelectedCompagny( {
            ...selectedCompagny,
            data: {
                [selectedDepart]: {
                    ...Object.fromEntries(
                        Object.entries(selectedCompagny.data[selectedDepart]).filter(
                            ([arriveLabel, arrivePrice]) => arriveLabel != arriveId
                        )
                    )
                }
            }
        })

    }

    function deleteDepart(departId){

        setSelectedCompagny({
            ...selectedCompagny,
            data: Object.fromEntries(
                Object.entries(selectedCompagny.data).filter(([label, subDatas]) => {
                    return departId != label;
                })
            )
        })

        if(departId == selectedDepart)
            setSelectedDepart();

    }

    function updateCompagny(){

        fakeDatabase.updateCompagny(selectedCompagny)

    }

    React.useEffect(()=>{
        setSelectedCompagny(compagnyProps)
    }, [compagnyProps]);

    return (

        <div className={'row no-gutters'}>

            <div className="col-12">
                {/*Modification des trajets de la ville {selectedCompagny.label}*/}
            </div>

            <div className="col-6">

                <div className="card">

                    <div className="card-body">

                        <button onClick={()=>{
                            updateCompagny();
                        }} className="btn btn-success">Enregistrer les modifications</button>

                        <h5>Departs</h5>

                        <div>
                            <input type="text" className="form-control" onChange={(event) => {
                                setDepartInput(event.target.value);
                            }}/>
                            <button onClick={addDepart} className="btn btn-primary">Ajouter</button>
                        </div>

                        <hr/>

                        <div>

                            {
                                selectedCompagny.data && Object.entries(selectedCompagny.data).map(
                                    ([label, data]) => {
                                        return (
                                            <div className={'d-flex'}>

                                                <div className={'flex-grow-1'}>{label}</div>

                                                <button onClick={()=>deleteDepart(label)} className={'btn btn-sm btn-danger btn-sm'}>Suppr.</button>
                                                <button onClick={()=>setSelectedDepart(label)} className={'btn btn-sm btn-primary btn-sm'}>Modifier</button>

                                            </div>
                                        )
                                    }
                                )
                            }

                        </div>

                    </div>

                </div>

            </div>

            <div className="col-6">


                {
                    selectedDepart && (
                        <div className="card bg-white">
                            <div className="card-body">

                                <div>

                                    <h5>
                                        Destination possible à partir de
                                    </h5>
                                    <h3 className={'text-center'}>{selectedDepart}</h3>


                                    <div>
                                        <input placeholder={'Nom de la ville'} type="text" className="form-control" onChange={(event)=>{
                                            setArriveInput(event.target.value)
                                        }}/>
                                        <input placeholder={'Coût du trajet'} type="text" className="form-control" onChange={(event)=>{
                                            setTrajetCoutInput(event.target.value)
                                        }}/>

                                        <button onClick={()=>{addArrive(selectedDepart, arriveInput, trajetCoutInput)}} className="btn btn-primary">Ajouter</button>

                                    </div>

                                    <hr/>

                                    {
                                        arrives && Object.entries(arrives).map(
                                            ([label, prix]) => {

                                                return (
                                                    <div>

                                                        <div className={'d-flex flex-row align-items-center'}>

                                                            <div className={'mx-4'}>
                                                                {label} | {prix} F CFA
                                                            </div>

                                                            <button onClick={()=>{deleteArrive(label)}} className="btn btn-danger btn sm">Suppr.</button>

                                                        </div>

                                                        <hr/>

                                                    </div>
                                                )
                                            }
                                        )
                                    }

                                </div>

                            </div>
                        </div>
                    )
                }

            </div>

        </div>

    )
}
import React from 'react';
import fakeDatabase from "../../../FakeDatabase/FakeDatabase";
import VilleTrajetForm from "./VilleTrajetForm";


export default function CompagniesForm(){

    const [villeInputLabel, setVilleInputLabel] = React.useState();
    const [villeInputContacts, setVilleInputContacts] = React.useState();
    const [compagnies, setCompagnies] = React.useState();

    const [selectedCompagny, setSelectedCompagny] = React.useState();

    function addVille(){

        if(!villeInputLabel && !villeInputContacts){

            alert('Vous devez saisir le libellé de la ville et un contact');

            return;
        }

        console.log('add villes')
        fakeDatabase.addCompagnie({
            label: villeInputLabel,
            contacts: villeInputContacts,
            departs: []
        })

        loadVilles();

    }

    function deleteVille(ville){

        fakeDatabase.deleteCompagny(ville.id)

        loadVilles();

    }

    function loadVilles(){

        const villes = fakeDatabase.getCompagnies();

        setCompagnies(villes);

    }

    React.useEffect(()=>{
        loadVilles();
    }, [])

    return (
        <div className="row">

            <div className="col-3">

                <div className="card bg-white">

                    <div className="card-body">

                        <h5 className={'font-weight-bolder'}>Compagnies</h5>
                        <div className={'small'}>Ajoutez supprimez des compagnies</div>

                        <hr/>

                        <div className="text-right">
                            <input onChange={(event) => setVilleInputLabel(event.target.value)} placeholder={'Libellé'} className={'form-control my-1'} />
                            <input onChange={(event) => setVilleInputContacts(event.target.value)} placeholder={'Contacts'} className={'form-control my-1'} />
                            <button onClick={addVille} className={'btn btn-block d-block btn-sm btn-primary'}>Ajouter</button>
                        </div>

                        <hr/>
                        <div>

                            {
                                compagnies && compagnies.map(
                                    ville => (
                                        <div>
                                            <div className={'d-flex flex-row my-2 align-items-center'}>

                                                <div className="flex-1 flex-grow-1">
                                                    {ville.label}
                                                    <div className={'text-small small font-weight-bold'}>Contacts:{ville.contacts || '-'}</div>
                                                </div>

                                                <div>

                                                    <button onClick={()=>{setSelectedCompagny(ville)}} className={'btn btn-sm btn-primary'}>
                                                        modif.
                                                    </button>
                                                    <button onClick={()=>{deleteVille(ville)}} className={'btn btn-sm btn-danger'}>
                                                        Suppr.
                                                    </button>

                                                </div>

                                            </div>

                                            <hr className={'my-2'}/>

                                        </div>
                                    )
                                )
                            }

                        </div>

                    </div>

                </div>

            </div>

            <div className="col-9">
                {selectedCompagny && (
                    <VilleTrajetForm selectedCompagny={selectedCompagny} compagnies={compagnies} />
                )}
            </div>

        </div>
)
}
import React from 'react';
import {Routes, Route} from 'react-router-dom'
import SideBar from "../Dashboard/SideBar/SideBar";
import MenuAdmain from "../Dashboard/DashboardAdmin/MenuAdmin/MenuAdmain";



export default function AdminTemplate(){

    return (
        <div>
            <SideBar />
            <MenuAdmain />
        </div>
    )
}
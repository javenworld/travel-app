import React from 'react'
import {Link} from 'react-router-dom'
import './Nav.css'
import useConnectedUser from "../../hooks/useConnectedUser";
import {useDispatch} from "react-redux";
import {deconnectUser} from "../../stores/authStore";

function Nav() {

    const user = useConnectedUser();
    const dispatch = useDispatch();

    function logout(){
        dispatch(deconnectUser());
    }

    return (
        <>

            <nav className='nav-bar'>
                <span>🏡⛪</span>
                <ul>
                    <Link to="/">
                        <li className='home'>🏠Home</li>
                    </Link>
                    <Link to="/About">
                        <li className='about'>📝About</li>
                    </Link>

                    {
                        !user && (
                            <>
                                <Link to="/Login">
                                    <li className='login'>👩🏽Login</li>
                                </Link>
                                <Link to="/SignUp">
                                    <li className='signUp'>Sign Up</li>
                                </Link>
                            </>
                        )
                    }

                    {
                        user && (
                            <>
                                <Link to="/dashboard/">
                                    <li className='login'>{user.name} {user.surname}</li>
                                </Link>
                                <a href="#" onClick={()=>{
                                    logout()
                                }}>
                                    <li className='signUp'>Déconnexion</li>
                                </a>
                            </>
                        )
                    }

                </ul>
            </nav>
            
        </>
    )
}

export default Nav

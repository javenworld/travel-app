import React from 'react';
import {useLocation} from "react-router-dom";

export default function useRouteMatcher(){

    const location = useLocation();

    console.log(location);

    return (path) => {
        return location.pathname.startsWith(path);
    }
}
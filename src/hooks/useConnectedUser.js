import React from 'react';
import {useSelector} from "react-redux";


export default function useConnectedUser(){

    const user = useSelector(
        state => state.authStore.user
    )

    return user;
}
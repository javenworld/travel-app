import React from 'react';
import useConnectedUser from "./useConnectedUser";


export default function useUserHasRoles(){

    const user = useConnectedUser();

    return (roles) => {
        
        for(let role of user.roles){
            if(roles.includes(role))
                return true;
        }

        return false;
    }
}
import React from 'react';
import fakeDatabase from "../../FakeDatabase/FakeDatabase";


export default function useVilleData(filters = {}){

    const [villes, setVilles] = React.useState([]);

    function loadVilles(){

        const villes = fakeDatabase.getVilles();
        setVilles(villes);

    }

    React.useEffect(()=>{
        loadVilles()
    }, [])

    return {
        villes,
        reload: loadVilles,
        errors: undefined
    }
}
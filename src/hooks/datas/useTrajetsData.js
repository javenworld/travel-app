import React from 'react';
import fakeDatabase from "../../FakeDatabase/FakeDatabase";
import useVilleData from "./useVilleData";


export default function useTrajetsData({departId} = {}){

    const {villes} = useVilleData();

    const [departs, setDeparts] = React.useState();
    const [arrives, setArrives] = React.useState();

    function loadArrives(departId){

        const trajets = fakeDatabase.getTrajets(departId);

        const arrives = trajets.map(
            item => {
                return villes.find(ville => ville.id == item.arriveId)
            }
        )

        console.log('allowed arrives', arrives, trajets, departId)

        setArrives(arrives);

    }

    React.useEffect(()=>{
        loadArrives(departId)
    }, [departId])

    return {
        departs: villes,
        arrives
    }
}
import React from 'react';
import fakeDatabase from "../../FakeDatabase/FakeDatabase";


export default function useCompagniesData(filters = {}){

    const [compagnies, setCompagnies] = React.useState([]);

    function loadCompagnies(){

        const compagnies = fakeDatabase.getCompagnies();
        setCompagnies(compagnies);

    }

    React.useEffect(()=>{
        loadCompagnies()
    }, [])

    return {
        compagnies,
        reload: loadCompagnies,
        errors: undefined
    }
}